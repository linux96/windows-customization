New-Itemproperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\ -name TaskbarSmallIcons -Value 1 -PropertyType DWord
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme -Value 0
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name EnableTransparency -Value 0
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name Colorprevalence -Value 1

function Set-TaskbarLocation {
    param(
        [Parameter(Mandatory)]
        [ValidateSet("left", "right", "top", "bottom")]
        $Location,
        [Parameter()]
        [Switch]$RestartExplorer
    )
    $bit = 0;
    switch ($Location) {
        "left" { $bit = 0x00 }
        "right" { $bit = 0x02 }
        "top" { $bit = 0x01 }
        "bottom" { $bit = 0x03 }
    }
    $Settings = (Get-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\StuckRects3 -Name Settings).Settings
    $Settings[12] = $bit
    Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\StuckRects3 -Name Settings -Value $Settings
    if ($RestartExplorer) {
        Get-Process explorer | Stop-Process
    }
}
Set-TaskbarLocation -Location Top -restartExplorer
